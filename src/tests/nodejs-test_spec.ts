/* eslint-disable class-methods-use-this */
import {MyTest} from "../mytest";


class _ThisTest extends MyTest
{
    public constructor()
    {
        super("node.js test");
    }

    protected registerTests(): void
    {
        const self = this;
        self.registerTest(
            "Successful node.js test",
            self._testOne.bind(self)
        );
        self.registerTest(
            "Failing node.js test",
            self._testTwo.bind(self),
            true
        );
        self.registerTest(
            "Successful promise node.js test",
            self._testThree.bind(self)
        );
        self.registerTest(
            "Failing promise node.js test",
            self._testFour.bind(self),
            true
        );
        self.registerTest(
            "Successful async node.js test",
            self._testFive.bind(self)
        );
        self.registerTest(
            "Failing async node.js test with throw",
            self._testSix.bind(self),
            true
        );
        self.registerTest(
            "Failing async node.js test with error",
            self._testSeven.bind(self),
            true
        );
    }

    private _testOne(): void
    {
        // A nice successful test :)
    }

    private _testTwo(): void
    {
        throw new Error("Am I failing? :O");
    }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    private async _testThree(): Promise<void>
    {
        // A nice successful test :)
        return Promise.resolve();
    }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    private async _testFour(): Promise<void>
    {
        return Promise.reject(Error("Am I failing? :O"));
    }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    private _testFive(done: (err?: Error) => void): void
    {
        // A nice successful test :)
        done();
    }

    private _testSix(_done: (_err?: Error) => void): void
    {
        throw new Error("All is broken!");
    }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    private _testSeven(done: (err?: Error) => void): void
    {
        done(new Error("Increasing WTF/sec!"));
    }
}

const _TEST = new _ThisTest();
_TEST.run();
