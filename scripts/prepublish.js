const childProcess = require("node:child_process");
const fs = require("node:fs");

function _readPackage()
{
    const file = fs.readFileSync('package.json');
    return JSON.parse(file);
}

function _checkBranch()
{
    const names = [
        "master",
        "main",
        "trunk"
    ];
    const branchName = childProcess.execSync("git rev-parse --abbrev-ref HEAD");
    if (!names.includes(branchName)) throw new Error("Not master branch");

    const branchTag = childProcess.execSync("git tag --points-at HEAD");
    if (branchTag === "") throw new Error("Missing tag");

    const package = _readPackage();
    if (package.version !== branchTag)
    {
        throw new Error("Branch and package.json tag mismatch");
    }
    console.log("Checks success!")
}

function _testInstall()
{
    childProcess.execSync("rm -fr node_modules");
    childProcess.execSync("npm install .");
    childProcess.execSync("npm audit --fix");
    console.log("Install success!");
}

function _build()
{
    childProcess.execSync("rm -fr dist");
    childProcess.execSync("npm run build");
    console.log("Build success!");
}

if (require.main === module)
{
    try
    {
        _checkBranch();
        _testInstall();
        _build();
    }
    catch (err)
    {
        console.error(String(err));
        process.exit(1);
    }
}
