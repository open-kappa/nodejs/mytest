/* eslint-disable class-methods-use-this */
import "should";
import {
    MyNodeRedTest,
    type TestFlowsItem
} from "../mytest";


class _ThisTest extends MyNodeRedTest
{
    public constructor()
    {
        super("node-red test", []);
    }

    protected registerFlows(): void
    {
        const self = this;
        const flowOne = self._flowOne.bind(self);
        self.registerFlow(
            "Successful node-red test",
            flowOne
        );
        self.registerFlow(
            "Failing node-red test",
            flowOne
        );
        self.registerFlow(
            "Successful promise node-red test",
            flowOne
        );
        self.registerFlow(
            "Failing promise node-red test",
            flowOne
        );
        self.registerFlow(
            "Successful async node-red test",
            flowOne
        );
        self.registerFlow(
            "Failing async node-red test with throw",
            flowOne
        );
        self.registerFlow(
            "Failing async node-red test with error",
            flowOne
        );
    }

    protected registerTests(): void
    {
        const self = this;
        self.registerTest(
            "Successful node-red test",
            self._testOne.bind(self),
            false
        );
        self.registerTest(
            "Failing node-red test",
            self._testTwo.bind(self),
            true
        );
        self.registerTest(
            "Successful promise node-red test",
            self._testThree.bind(self),
            false
        );
        self.registerTest(
            "Failing promise node-red test",
            self._testFour.bind(self),
            true
        );
        self.registerTest(
            "Successful async node-red test",
            self._testFive.bind(self)
        );
        self.registerTest(
            "Failing async node-red test with throw",
            self._testSix.bind(self),
            true
        );
        self.registerTest(
            "Failing async node-red test with error",
            self._testSeven.bind(self),
            true
        );
    }

    private _flowOne(): Array<TestFlowsItem>
    {
        const self = this;
        return [
            self.makeSinkNode("n0", "Nice guy.")
        ];
    }

    private _testOne(): void
    {
        // A nice successful test :)
        const self = this;
        const n0 = self.getNode("n0");
        n0.should.have.property("name", "Nice guy.");
    }

    private _testTwo(): void
    {
        false.should.be.true("Am I failing? :O");
    }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    private async _testThree(): Promise<void>
    {
        // A nice successful test :)
        const self = this;
        async function foo(): Promise<void>
        {
            const n0 = self.getNode("n0");
            n0.should.have.property("name", "Nice guy.");
            return Promise.resolve();
        }
        return foo();
    }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    private async _testFour(): Promise<void>
    {
        return Promise.reject(Error("Am I failing? :O"));
    }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    private _testFive(done: (err?: Error) => void): void
    {
        const self = this;
        const n0 = self.getNode("n0");
        function callMe(): void
        {
            done();
        }
        n0.on("input", callMe);
        n0.receive({});
    }

    private _testSix(_done: (_err?: Error) => void): void
    {
        // Failing with throw is allowed only into the synchronous part:
        throw new Error("All is broken!");
    }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    private _testSeven(done: (err?: Error) => void): void
    {
        const self = this;
        const n0 = self.getNode("n0");
        function callMe(): void
        {
            done(new Error("Increasing WTF/sec!"));
        }
        n0.on("input", callMe);
        n0.receive({});
    }
}

const _TEST = new _ThisTest();
_TEST.run();
