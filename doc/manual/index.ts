/* eslint-disable */

/**
 * @module -- Manual --
 */

export class Manual
{
    private constructor()
    {}

    /**
     * [[include:1.motivations.md]]
     */
    "1. Motivations": void;

    /**
     * [[include:2.write-nodejs-tests.md]]
     */
    "2. Write NodeJS tests": void;

    /**
     * [[include:3.write-node-red-tests.md]]
     */
     "3. Write Node-RED Tests": void;

    /**
     * [[include:4.advanced-tests.md]]
     */
    "4. Advanced tests": void;

    /**
     * [[include:5.contributing.md]]
     */
    "5. Contributing": void;

    /**
     * [[include:6.copyright.md]]
     */
    "6. Copyright": void;

    /**
     * [[include:7.changelog.md]]
     */
    "7. Changelog": void;
}
