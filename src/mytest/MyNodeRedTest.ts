import type {
    Node,
    NodeDef
} from "node-red";
import {MyTest} from "./mytestImpl";
import type nodeRedNodeTestHelper from "node-red-node-test-helper";

type _NodeTestHelper = typeof nodeRedNodeTestHelper;
export type TestFlowsItem<TNodeDef extends NodeDef = NodeDef> =
    nodeRedNodeTestHelper.TestFlowsItem<TNodeDef>;
export type TestNodeInitializer = nodeRedNodeTestHelper.TestNodeInitializer;

// - @TODO Support reading of external json flows.

/**
 * Base class for node-red tests.
 */
export abstract class MyNodeRedTest
    extends MyTest
{
    /** The default name for the flow tab */
    private static get _DEFAULT_TAB(): string
    {
        return "DefaultTab";
    }

    /** The map of registered flows. */
    private readonly _flows: Map<string, Array<TestFlowsItem>>;
    /** Reference to global helper. */
    private readonly _helper: _NodeTestHelper;
    /** Flag to trace the need to call helper.unload(). */
    private _isLoaded: boolean;
    /** The list of node models registered in nodered. */
    private readonly _nodeModels: TestNodeInitializer;

    /**
     * Constructor.
     * @param groupName - The name for this group of tests.
     * @param nodeModels - The list of node
     *     models to register in nodered.
     */
    public constructor(
        groupName: string,
        nodeModels: TestNodeInitializer
    )
    {
        super(groupName);
        this._nodeModels = nodeModels;
        this._isLoaded = false;
        this._flows = new Map<string, Array<TestFlowsItem>>();

        try
        {
            const helper =
                // eslint-disable-next-line max-len
                // eslint-disable-next-line @typescript-eslint/no-require-imports, @typescript-eslint/no-var-requires
                require("node-red-node-test-helper") as _NodeTestHelper;
            // Required once init of helper instance.
            helper.init(require.resolve("node-red"));
            this._helper = helper;
        }
        catch
        {
            throw new Error("Please install peer dependencies");
        }
    }

    /**
     * Get the node with given ID.
     * @typeParam TCreds - The node credentials type.
     * @param id - The node ID.
     */
    public getNode<
        TCreds extends {[key: string]: unknown} = {[key: string]: unknown}
    >(
        id: string
    ): Node<TCreds>
    {
        const self = this;
        return self._helper.getNode(id) as Node<TCreds>;
    }

    /**
     * Create a node-red catch node.
     * @typeParam TNodeDef - The node definition parameters.
     * @param id - The unique node ID.
     * @param name - The user-friendly node name.
     * @param wires - The node output wires.
     * @param onlyUncaughtByOthers - Optional. True if t catches only
     *     errors uncaught by other nodes.
     * @param scope - Optional. List of node ids to which
     *     this catch refers to.
     * @param other - Other possible node-specific
     * @returns The node for the flow.
     */
    public makeCatchNode<TNodeDef extends NodeDef = NodeDef>(
        id: string,
        name: string,
        wires: Array<Array<string>>,
        onlyUncaughtByOthers = false,
        scope: Array<string> | null = null,
        other: Partial<TestFlowsItem<TNodeDef>> = {}
    ): TestFlowsItem<TNodeDef>
    {
        const self = this;
        const value = {
            ...other,
            scope,
            "uncaught": onlyUncaughtByOthers,
            wires
        };
        return self.makeNode(id, name, "catch", value);
    }

    /**
     * Create a node-red node.
     * @typeParam TNodeDef - The node definition parameters.
     * @param id - The unique node ID.
     * @param name - The user-friendly node name.
     * @param type - The node type name.
     * @param other - Other possible node-specific configurations.
     * @returns The node for the flow.
     */
    public makeNode<TNodeDef extends NodeDef = NodeDef>(
        id: string,
        name: string,
        type: string,
        other: Partial<TestFlowsItem> = {}
    ): TestFlowsItem<TNodeDef>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;

        const value: TestFlowsItem = {
            ...other,
            id,
            name,
            type
        };
        if (typeof value.z === "undefined")
        {
            value.z = MyNodeRedTest._DEFAULT_TAB;
        }
        return value as TestFlowsItem<TNodeDef>;
    }

    /**
     * Create a node-red sink node joint with a custom node.
     * @typeParam TNodeDef - The node definition parameters.
     * @param nodeId - The unique custom node ID.
     * @param nodeName - The user-friendly custom node name.
     * @param sinkId - The unique sink node ID.
     * @param sinkName - The user-friendly sink node name.
     * @param other - Optional other possible custom node
     *     settings.
     * @returns The nodes for the flow.
     */
    public makeNodeWithSink<TNodeDef extends NodeDef = NodeDef>(
        nodeId: string,
        nodeName: string,
        nodeType: string,
        sinkId: string,
        sinkName: string,
        other: Partial<TestFlowsItem<TNodeDef>> = {}
    ): Array<TestFlowsItem>
    {
        const self = this;
        const value = {...other};
        if (typeof value.wires === "undefined")
        {
            const obj = [] as unknown as TestFlowsItem<TNodeDef>["wires"];
            const obj1 = obj!;
            value.wires = obj1;
        }
        value.wires.push([sinkId]);
        const node = self.makeNode(nodeId, nodeName, nodeType, value);
        const sink = self.makeSinkNode(sinkId, sinkName);
        return [node, sink];
    }

    /**
     * Create a node-red source node joint with a custom node.
     * @typeParam TNodeDef - The node definition parameters.
     * @param sourceId - The unique sink node ID.
     * @param sourceName - The user-friendly sink node name.
     * @param nodeId - The unique custom node ID.
     * @param nodeName - The user-friendly custom node name.
     * @param other - Optional other possible custom node
     *     settings.
     * @returns The nodes for the flow.
     */
    public makeNodeWithSource<TNodeDef extends NodeDef = NodeDef>(
        sourceId: string,
        sourceName: string,
        nodeId: string,
        nodeName: string,
        nodeType: string,
        other: Partial<TestFlowsItem<TNodeDef>> = {}
    ): Array<TestFlowsItem>
    {
        const self = this;
        const source = self.makeSourceNode(sourceId, sourceName, [[nodeId]]);
        const node = self.makeNode(nodeId, nodeName, nodeType, other);
        return [source, node];
    }

    /**
     * Create a node-red source node joint with a custom node, which is
     * joint with a sink node.
     * @typeParam TNodeDef - The node definition parameters.
     * @param sourceId - The unique sink node ID.
     * @param sourceName - The user-friendly sink node name.
     * @param nodeId - The unique custom node ID.
     * @param nodeName - The user-friendly custom node name.
     * @param sinkId - The unique sink node ID.
     * @param sinkName - The user-friendly sink node name.
     * @param other - Optional other possible custom node settings.
     * @returns The nodes for the flow.
     */
    public makeNodeWithSourceAndSink<TNodeDef extends NodeDef = NodeDef>(
        sourceId: string,
        sourceName: string,
        nodeId: string,
        nodeName: string,
        nodeType: string,
        sinkId: string,
        sinkName: string,
        other: Partial<TestFlowsItem<TNodeDef>> = {}
    ): Array<TestFlowsItem>
    {
        const self = this;
        const source = self.makeSourceNode(sourceId, sourceName, [[nodeId]]);
        const ret = self.makeNodeWithSink(
            nodeId,
            nodeName,
            nodeType,
            sinkId,
            sinkName,
            other
        );
        ret.unshift(source);
        return ret;
    }

    /**
     * Create a node-red sink node.
     * @typeParam TNodeDef - The node definition parameters.
     * @param id - The unique node ID.
     * @param name - The user-friendly node name.
     * @param other - Other possible node-specific
     * @returns The node for the flow.
     */
    public makeSinkNode<TNodeDef extends NodeDef = NodeDef>(
        id: string,
        name: string,
        other: Partial<TestFlowsItem<TNodeDef>> = {}
    ): TestFlowsItem<TNodeDef>
    {
        const self = this;
        return self.makeNode(id, name, "helper", other);
    }

    /**
     * Create a node-red source node.
     * @typeParam TNodeDef - The node definition parameters.
     * @param id - The unique node ID.
     * @param name - The user-friendly node name.
     * @param wires - The node output wires.
     * @param other - Other possible node-specific
     * @returns The node for the flow.
     */
    public makeSourceNode<TNodeDef extends NodeDef = NodeDef>(
        id: string,
        name: string,
        wires: Array<Array<string>>,
        other: Partial<TestFlowsItem<TNodeDef>> = {}
    ): TestFlowsItem<TNodeDef>
    {
        const self = this;
        const value = {
            ...other,
            wires
        };
        return self.makeNode(id, name, "helper", value);
    }

    /**
     * Creates a tab in the flow.
     * @param id - The tab ID
     */
    public makeTab(id: string): TestFlowsItem
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;

        const ret: TestFlowsItem & {"label": string} = {
            "id": id,
            "label": id,
            "type": "tab"
        };
        return ret;
    }

    /**
     * Register a new flow for the given test.
     * @param name - The user-friendly test name.
     * @param makeFlow - The class instance method to create the test flow.
     */
    public registerFlow(
        name: string,
        makeFlow: () => Array<TestFlowsItem>
    ): void
    {
        const self = this;
        let flow = makeFlow.call(self);
        flow = [self.makeTab(MyNodeRedTest._DEFAULT_TAB)].concat(flow);
        self._flows.set(name, flow);
    }

    /**
     * Runs the tests.
     * Reimplemented from parent.
     */
    public run(): void
    {
        const self = this;
        self.registerFlows();
        super.run();
    }

    /**
     * Generate a wrapper to accomodate the testExecutor.
     * Reimplemented from parent.
     * @param name - The user-friendly test name.
     * @param testExecutor - A standalone function which
     *     wraps the test execution.
     * @returns  The wrapper.
     */
    protected _generateWrapper(
        name: string,
        testExecutor: () => Promise<void>
    ): () => Promise<void>
    {
        const self = this;

        async function setLoaded(): Promise<void>
        {
            self._isLoaded = true;
            return Promise.resolve();
        }

        async function ret(): Promise<void>
        {
            const flow = self._flows.get(name);
            if (typeof flow === "undefined")
            {
                return Promise.reject(
                    new Error(`Unexpected flow name: ${name}`)
                );
            }
            return self._helper.load(self._nodeModels, flow, {})
                .then(setLoaded)
                .then(testExecutor);
        }
        return ret;
    }

    /**
     * Prepare the given test execution.
     * Reimplemented from parent.
     * @param name - The user-friendly test name.
     * @returns Promise to signal the preparation is completed.
     */
    protected async _prepareSingleTest(name: string): Promise<void>
    {
        const self = this;
        if (!self._flows.has(name))
        {
            return Promise.reject(Error("Missing flow for test"));
        }
        return Promise.resolve();
    }

    /**
     * Callback executed after each test.
     * Stops the node-red server and unloads the configuration, if required.
     * Reimplemented from parent.
     * @param done - The callback to execute when completed.
     */
    protected async afterEachTest(): Promise<void>
    {
        const self = this;
        async function unload(): Promise<void>
        {
            if (self._isLoaded) return self._helper.unload();
            return Promise.resolve();
        }
        function impl(
            resolve: () => void,
            _reject: (err: Error) => void
        ): void
        {
            self._helper.stopServer(resolve);
        }
        async function stopServer(): Promise<void>
        {
            return new Promise(impl);
        }
        return unload().then(stopServer);
    }

    /**
     * Callback executed before each test.
     * Starts the node-red server.
     * Reimplemented from parent.
     * @param done - The callback to execute when completed.
     */
    protected async beforeEachTest(): Promise<void>
    {
        const self = this;
        function impl(
            resolve: () => void,
            _reject: (err: Error) => void
        ): void
        {
            self._helper.startServer(resolve);
        }
        return new Promise(impl);
    }

    /**
     * Register the flows creation methods for all the tests.
     * Call registerFlow() to assolve this purpose.
     */
    protected abstract registerFlows(): void;
}
