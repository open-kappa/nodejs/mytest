/**
 * Base class for all tests.
 * Represents a group of tests.
 * Internally it clean up the argv from mocha parameters, by searching for a
 * double dash (--), and assuming the instantiating file as the test file name.
 * Test parameters can start with a "\@" to be unrecognized by mocha.
 */
export abstract class MyTest
{
    /**
     * Whether to apply the command line workaround for mocha tests.
     * Default is true.
     */
    private static _doCommandLineFix = true;
    /**
     * The original command line.
     */
    private static readonly _ORIGINAL_COMMAND_LINE = [...process.argv];

    /** Whether an error occurred. */
    private _errorOccurred: boolean;
    /** The user-friendly name of this group of tests. */
    private readonly _groupName: string;
    /** Whether to halt on first failing test of the current group. */
    private _haltOnFirstError: boolean;

    /**
     * Constructor.
     * @param groupName - The user-friendly name of this group of tests.
     */
    public constructor(groupName: string)
    {
        this._errorOccurred = false;
        this._groupName = groupName;
        this._haltOnFirstError = false;
        this._cleanupCommandLine();
    }

    /**
     * Set whether to perform the command line fix.
     * @param doFix - Whether to perform the fix.
     */
    public static setPerformCommandLineFix(doFix: boolean): void
    {
        MyTest._doCommandLineFix = doFix;
    }

    /**
     * Register a member method to be executed as test.
     * Useful to implement registerTests().
     * @param name - The user friendly test name.
     * @param foo - The member method implementing the test.
     * @param mustFail - Optional. True if test is expected to fail.
     * @param haltOnError - Optional. True if test failure implies
     *     halting and not executing other tests.
     */
    public registerTest(
        name: string,
        foo: (() => Promise<void> | void)
        | ((done: (err?: Error) => void) => Promise<void> | void),
        mustFail = false,
        haltOnError = false
    ): void
    {
        const self = this;
        let ret: Promise<void> | null = null;
        function promiseImpl(
            resolve: () => void,
            reject: (reason: unknown) => void
        ): void
        {
            function done(err?: Error): void
            {
                if (Boolean(err)) reject(err);
                else resolve();
            }

            try
            {
                const newRet = foo.call(self, done);
                if (newRet instanceof Promise) ret = newRet;
                if (foo.length === 0) done();
            }
            catch (err: unknown)
            {
                reject(err);
            }
        }
        async function chainRet(): Promise<void>
        {
            if (ret === null) return Promise.resolve();
            return ret;
        }
        async function testExecutor(): Promise<void>
        {
            const prom = new Promise<void>(promiseImpl);
            return prom
                .then(chainRet);
        }
        self._registerTest(name, testExecutor, mustFail, haltOnError);
    }

    /**
    * Execute the registered tests.
    */
    public run(): void
    {
        const self = this;
        self._setupGroup();
    }

    /**
     * Set whether to halt on first error.
     * @param halt - True to halt.
     */
    public setHaltOnFirstError(halt: boolean): void
    {
        this._haltOnFirstError = halt;
    }

    /**
     * Generate a wrapper to accomodate the testExecutor.
     * Reimplemented from parent.
     * @param _name - The user-friendly test name.
     * @param testExecutor - A standalone function which wraps the test
     *     execution.
     * @returns The wrapper.
     */
    protected _generateWrapper(
        _name: string,
        testExecutor: () => Promise<void>
    ): () => Promise<void>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        return testExecutor;
    }

    /**
     * Prepare the given test execution.
     * @param _name - The user-friendly test name.
     * @returns Promise to signal the preparation is completed.
     */
    protected async _prepareSingleTest(_name: string): Promise<void>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        return Promise.resolve();
    }

    /**
     * Callback executed when all tests have completed.
     * This implementation does nothing.
     * @returns The completion promise.
     */
    protected async after(): Promise<void>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        return Promise.resolve();
    }

    /**
     * Callback executed after each test.
     * This implementation does nothing.
     * @returns The completion promise.
     */
    protected async afterEachTest(): Promise<void>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        return Promise.resolve();
    }

    /**
     * Callback executed at begining of testing.
     * This implementation does nothing.
     * @returns The completion promise.
     */
    protected async before(): Promise<void>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        return Promise.resolve();
    }

    /**
     * Callback executed before each test.
     * This implementation does nothing.
     * @returns The completion promise.
     */
    protected async beforeEachTest(): Promise<void>
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;
        return Promise.resolve();
    }

    /**
     * Register all the tests.
     * Call registerTest() to assolve this purpose.
     * Registered tests are executed in order of registration.
     */
    protected abstract registerTests(): void;

    /**
     * Cleanup the command line.
     */
    private _cleanupCommandLine(): void
    {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const self = this;

        if (!MyTest._doCommandLineFix) return;

        function newPrepareStack(
            _err: Error,
            stackTraces: Array<NodeJS.CallSite>
        ): Array<NodeJS.CallSite>
        {
            return stackTraces;
        }
        function getCallerFile(): string
        {
            const originalFunc = Error.prepareStackTrace;
            let callerFile = "";
            try
            {
                const err = new Error();

                Error.prepareStackTrace = newPrepareStack;
                const stack = err.stack as unknown as Array<NodeJS.CallSite>;
                const currentFile = stack.shift()?.getFileName();

                while (stack.length !== 0)
                {
                    const newFile = stack.shift()?.getFileName();
                    if (currentFile === newFile) continue;
                    callerFile = newFile ?? "";
                    break;
                }
            }
            catch
            {
                // ntd
            }

            Error.prepareStackTrace = originalFunc;
            return callerFile;
        }

        function getArgs(argv: Array<string>): Array<string>
        {
            const index = argv.indexOf("--");
            if (index === -1) return [];
            const ret = argv.slice(index + 1);
            for (let i = 0; i < ret.length; ++i)
            {
                const param = ret[i];
                if (!param.startsWith("@")) continue;
                ret[i] = param.substring(1);
            }
            return ret;
        }

        const argv = [MyTest._ORIGINAL_COMMAND_LINE[0]];
        const programName = getCallerFile();
        const args = getArgs(MyTest._ORIGINAL_COMMAND_LINE);
        argv.push(programName);
        argv.push(...args);
        process.argv = argv;
    }

    /**
     * Call the given singleTest() and manage the result.
     * @param singelTest - A standalone method, which wraps
     *     _executeSingleTest().
     * @param mustFail - True if test is expected to fail.
     * @param haltOnError - Optional. True if test failure implies
     *     halting and not executing other tests.
     * @returns The completion promise.
     */
    private async _executeSingleTest(
        singelTest: () => Promise<void>,
        mustFail: boolean,
        haltOnError: boolean
    ): Promise<void>
    {
        const self = this;
        if (self._haltOnFirstError && self._errorOccurred)
        {
            return Promise.reject(new Error("not run"));
        }
        let doCatch = true;
        async function onSuccess(): Promise<void>
        {
            if (mustFail)
            {
                self._errorOccurred = true;
                if (haltOnError) self._haltOnFirstError = true;
                doCatch = false;
                return Promise.reject(
                    new Error("test was expected to fail, but did not.")
                );
            }
            return Promise.resolve();
        }

        async function onError(err: Error): Promise<void>
        {
            if (mustFail && doCatch) return Promise.resolve();
            self._errorOccurred = true;
            if (haltOnError) self._haltOnFirstError = true;
            return Promise.reject(err);
        }

        return singelTest()
            .then(onSuccess)
            .catch(onError);
    }

    /**
     * Register the member callbacks within mocha.
     */
    private _registerHooks(): void
    {
        const self = this;
        async function ba(): Promise<void>
        {
            return self.before();
        }
        async function aa(): Promise<void>
        {
            return self.after();
        }
        async function bef(): Promise<void>
        {
            return self.beforeEachTest();
        }
        async function aef(): Promise<void>
        {
            return self.afterEachTest();
        }
        /* eslint-disable no-undef */
        before(ba);
        after(aa);
        beforeEach(bef);
        afterEach(aef);
        /* eslint-enable no-undef */
    }

    /**
     * Register a standalone method to be executed as test.
     * The test must execute asynchronously, resolving a promise.
     * Internally, chains _prepareSingleTest() with testExecutor().
     * @param name - The user friendly test name.
     * @param testExecutor - The method implementing the test.
     * @param mustFail - True if test is expected to fail.
     * @param haltOnError - True if test failure implies
     *     halting and not executing other tests.
     */
    private _registerTest(
        name: string,
        testExecutor: () => Promise<void>,
        mustFail: boolean,
        haltOnError: boolean
    ): void
    {
        const self = this;
        const wrapper = self._generateWrapper(name, testExecutor);
        async function singleTest(): Promise<void>
        {
            return self._executeSingleTest(wrapper, mustFail, haltOnError);
        }
        async function prepareTest(): Promise<void>
        {
            return self._prepareSingleTest(name).then(singleTest);
        }
        // eslint-disable-next-line no-undef
        it(`${self._groupName}: ${name}`, prepareTest);
    }

    /**
     * Do the group setup.
     */
    private _setupGroup(): void
    {
        const self = this;
        function thisTest(): void
        {
            self._setupTests();
        }
        // eslint-disable-next-line no-undef
        describe(self._groupName, thisTest);
    }

    /**
     * Setup all the tests.
     */
    private _setupTests(): void
    {
        const self = this;
        self._registerHooks();
        self.registerTests();
    }
}
